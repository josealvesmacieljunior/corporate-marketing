##### 🛑  This is a confidential issue because of the nature of our conversation.

## Background
[insert the crisis background and its' outcome here; remember that folks who were not active will want to review, so be descriptive and link to issues, epics, slack messsages, etc.]

## Our core objective was [insert objective here] 
[insert details around the objective, how it was met or not]

## How recommendations came together
[insert the process taken to organize the strategy; was it a documented process? did it happen on Slack in an emergency?] 

##### Resources reviewed in the landscape (competitors, partners, and global top brands)
[insert bulleted list of other brands we've identified going through a similar crisis and what we observed them doing]

## What happened (please add more to the lists below if you can)
##### Positives
- [inserted bulleted list here]
##### Negatives
- [inserted bulleted list here]
##### Opportunities
- [inserted bulleted list here]
- [opportunities are action items we can iterate on for the future]

## Materials to Use in Review
- [inserted bulleted list with links here; consider docs, slack messages, other issues/epics, anything that helped to organize our response]

<!-- Leave the section below for issue tags
/label ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"
/confidential
 -->
