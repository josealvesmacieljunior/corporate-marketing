<!-- To be completed by the Corp Comms team. Some details may not be applicable.--> 
*Note: The press release development and review process should be kicked off at least 2 weeks prior to the planned announcement date to allow for sufficient time to get content/messaging developed and routed through the proper approval process with all key DRIs, IR and legal.*

#### :question: Background on the Announcement

*Insert details on the purpose of the press release and announcement including product, partnership, event (etc.) details, requirements/materials needed for the launch, key stakeholders, etc.*

#### :calendar: Date/Time of Announcement and Embargo

*Insert date/time here.*

#### :notepad_spiral: Press Release DRAFT

*Include link to the G Doc of the working press release draft.*

#### :books: Helpful Resources/Content for the Announcement

*Include links to related content, analyst reports, survey data (if applicable) and messaging.*

#### :mega: Coverage

*Complete after announcement.*

------
##### Press Release Approvals:
* [ ] Corp Comms team
* [ ] DRI of the Announcement
* [ ] PMM/partner marketing DRI (if applicable)
* [ ] Product DRI (if applicable)
* [ ] Spokesperson Quoted
* [ ] IR
* [ ] Legal

/label ~"corporate marketing" ~"Corp Comms" ~"PR" ~"press release" ~"mktg-status::plan"

/cc @nwoods1 @cweaver1 @JMLeslie

/confidential
