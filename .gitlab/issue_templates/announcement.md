### Request for announcement of **add your news here**

<!-- Requester please fill in all relevant sections above the solid line. 
Some details may not be applicable. Please see the handbook for more information 
on requesting an announcement: https://about.gitlab.com/handbook/marketing/corporate-marketing/#pr-requests-for-announcements. --> 

### Details

#### What is the announcement?

[add details here]

#### Who is the primary audience for the announcement?

[add details here]

#### When do you want to share the announcement, ideally?

[add details here]

#### Is this announcement/news tied to a specific industry or partner event? If so, what is the date of the event?

[add details here]

#### Is this announcement/news being driven/led by GitLab, partner or customer? 
 
If not GitLab led, what company is driving the announcement?
  
[add details here]

#### Who is the GitLab DRI for the announcement? 

[add details here]

#### Any additional GitLab SMEs who need to be a part of the review/approval process?

[add details here]

####  Link related issues and epics
   - [ ] Please link to all related issues and the epic

------
##### To-Do's
##### Step 1: `PR Team Member`, please determine announcement tier or recommended communications channel
* [ ] Tier 1 (press release, optional blog, social amp)
* [ ] Tier 2 (blog, social amp)
* [ ] Tier 3 (potential social amp)
* [ ] Other (please specify)

Indicate if PR team and/or Legal need to approve announcement before publishing on selected channel:
* [ ] Approval required

##### Step 2: For `Requester` to complete after tier/communications channel has been determined
###### Social Media
* [ ] If social amp in addition to routine promotion of new content is required, please open a new [social-general-request issue template in the Corporate Marketing Project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?issuable_template=social-general-request), tag `PR Team Member` who determined tier, and link issues.
   - _Note, all blogs are shared on our social channels by default. Opening a separate issue is best when there are specific timelines of promotion or some type of unique ask beyond simply sharing the blog._
* [ ] If a customer/partner is leading/participating in the announcement, *please encourage them to tag our company pages on every channel they are pushing the news.* [Links to all our channels are here](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#primary-social-channels-audiences-and-calendaring-). This makes engagement and amplifcation of their content easier, and helps GitLab social channels grow.

###### Blog Post
* [ ] If a blog post is required, please open a blog post issue and ping `@rebecca`. Instructions are in the [blog handbook](https://about.gitlab.com/handbook/marketing/blog/#time-sensitive-posts--instructions)
  * [ ] Mark blog post issue as [related](https://docs.gitlab.com/ee/user/project/issues/related_issues.html#adding-a-related-issue) to this one

Note, that if there is a need for pictures or other visuals to be created, this process is outlined in the social issue and requires more time.\
Be sure to [consider the totality of your request for social amp](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#please-remember).

/label ~"corporate marketing" ~"announcement" ~"Corp Comms" ~"PR" ~"mktg-status::plan"

/cc @nwoods1 @cweaver1 @JMLeslie @KatieHWPR @Fabzzz @kpdoespr

/confidential
