<!-- Purpose of this issue: To create a Marketo program, SFDC campaign, (zoom when applicable) and sync. -->
<!-- This issue type is ONLY to be opened by members of the Field Marketing Team. -->

## Action Items Checklist
* [ ] Name this issue `Program Tracking: [name of campaign]` (ex. Program Tracking: Modernize CI/CD Webcast)
* [ ] Update milestone, due date, and assignee in project management section at bottom of issue
* [ ] Create Marketo program - [process to follow](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up)
* [ ] Create Salesforce campaign - [process to follow](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up)
* [ ] Provide type of Marketo landing page (select one)- if applicable
   - [ ] Buyer Progression Webcast
   - [ ] Partner Webcast
   - [ ] Virtual Workshop
* [ ] Add Marketo and SFDC links to Epic
* [ ] Copy: `add link to copy googledoc - be sure it is editable by all GitLab`
* [ ] Reviewers/Approvers: `@gitlab-handle of relevant individuals`
* [ ] When copy is final and reviewed, close this issue, and place final copy in the **Marketo LP and Automation issue**
* Close out this issue.


<!-- DO NOT UPDATE - PROECT MANAGEMENT
/epic 
/weight 1
/label ~"mktg-status::wip" ~"Corporate Event"
-->
