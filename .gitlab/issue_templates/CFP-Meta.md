*Title: CFP Meta: [Event Name, Location, Event Dates] - Due [CFP Due Date]*

## Event Details

* Name of event: `GitLab Commit`
* Event website: https://about.gitlab.com
* Location: `Virtual`
* Event Date: `September 21–24, 2020`
* CFP Deadline: `April 24, 2020`
* CFP Submission Link: https://about.gitlab.com/cfp
* GitLab Event Issue/Epic: `Link to Issue if available`

## CFP Description

*Detailed Description of the CFP*


### CFP Submission Guidelines
**Important: Please use the [CFP submission issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission) to submit for this event.**

*Guidelines to follow for submission, please include sample submission template if available*


## DE Checklist

* [ ] Internally Promoted
* [ ] Speaker Sourcing
* [ ] Community Reachout
* [ ] Talks Submitted (`0`)


For your input please:  @johncoghlan @dnsmichi @brendan @abuango @Emily

/label ~Events ~Speaker ~SPIFF ~"mktg-status::plan" ~"dev-evangelism" ~"DE-CFP" ~"DE-CFP-Meta::Open"

<!-- Please leave the label below on this issue -->
