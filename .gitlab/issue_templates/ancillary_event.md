## Request an ancillary event:
To only be used if meta issue created and doing side/ancillary events related to main event that need **separate campaign tags and tracking**

* Link to [Main Event Issue]()
Make sure to relate to EPIC

 ---
## :paperclip: Event Planning Key Docs
* [Epic]() Add Epic Link here- to be added by MPM
* [Event Planning Sheet]() copy [this doc](https://docs.google.com/spreadsheets/d/1rXzp1bLoiI-IJ86Jbe39Kif2iujQoWVNXsbxQisFwUY/edit#gid=1085564346) and create own version for specific event
* [Budget Doc](https://docs.google.com/spreadsheets/d/1R5wJuG8H-DfgkbSOJYhGs-YvTEGFR2DZX9KN_VegG7o/edit?ts=5de975d1#gid=0) - `It is the responsibilty of the DRI to keep the budget doc up-to-date and to notify their manager as soon as or if you think you will go over budget` 
* `Finance Tag(s)` - create the 'campaign tag' (using proper ISOdate_name, noting 37 character limit) then add into the associated budget line item. Each campaign that needs tracking should get it's own tag. 

## :notepad_spiral: Event Details 
* **Event DRI:** CEM Name @corpeventusername
* **MPM:** @mpmusername
* **Type:** Owned Event, Field Event, etc. Other - see handbook for event type definitions
* **Ancillary event:** (for example: Speaking session, Workship, Dinner, Party, etc.)
* **Official Event Name:** 
* **Date:**
* **Time:**
* **Location:** 
* **Event Website:**
* **Expected Number of Attendees at the event:**
* **GitLab Staffing Needs:**
* **Dress Code/ Attire:**
* **GitLab Hosted Landing page (if applicable):**
* **Speakers (if applicable):**
* **SFDC Campaign:**
* **Attendee List/ Audience Demographics:**
* **If we went last year, link to SF.com campaign:** 
* **Event Goals:**
  * Goals for # of leads: 
  * Goals for pipeline created $$$: 
  * Number of meetings at event: 
  * registration goals/ attendance goals: 

## :moneybag: Financial
* **Campaign tag (requested & created by Corp Event DRI in **Finance Issue** - [ISOdate_Campaign_ShortName](https://about.gitlab.com/handbook/marketing/#campaign-tags)):**
* **Budgeted costs (sponsorship + auxiliary cost (AV, booth, swag, travel, printed materials...)):** `DRI to fill in`
* [ ]  Event [evaluated and aligned to regional and company goals](https://about.gitlab.com/handbook/marketing/events/#how-we-evaluate-and-build-potential-events)
* [ ]  Spend added to [Non Headcount Budget Sheet](https://docs.google.com/spreadsheets/d/1WVWZjSF6f5jAFqHO4hXcV8mN975ITT4eXScSn0F_FU8/edit#gid=0)
* [ ]  Finance issue created in [Issues · GitLab.com / Finance · GitLab](https://gitlab.com/gitlab-com/finance/issues)
* [ ]  Invoice received. [Instructions on procure to pay](https://about.gitlab.com/handbook/finance/accounting/#procure-to-pay). 
* [ ]  Invoice paid

## :level_slider: Event Overview/ Details 
* 
---
`**For Event Planners to complete for MPM once we know event is approved**`

## :white_check_mark: MPM request checklist  

#### What will this event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the FMM before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
* Will the copy rely on content from the event? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Add event leads to nurture stream? PLEASE NOTE: MPM will put all leads from the ancillary event into the same nurture as the main event
* Landing page creation (requires promotion plan) - YES/NO
* If yes for Landing page: Should this event be added to the [public events list](https://about.gitlab.com/events/) - YES/NO 
* Will the landing page contain a form for meeting/demo requests - YES/NO (if YES, choose meeting/demo)
* Invitation and reminder emails (requires promotion plan) - YES/NO
* Will this event require sales-nominated workflow? - YES/NO
* Alliances/Partner Marketing involved - YES/NO/MAYBE (if yes, must designate a DRI)
* Will this event include use of Marketing Development Funds (MDFs)? YES/NO
   * if yes, please tag Tina Sturgis here, and complete the section below
        * [ ]  MDF request completed by FMM and sent to Tina at least 1 quarter out
        * [ ]  event accepted for use of MDF (tag Tina here for her to approve)
        * [ ]  receipt submitted post event
        * [ ]  proof of payment submitted
        * [ ]  leads submitted 

---
`**For Event Planners complete once event contract signed- this is for our own internal tracking purposes**`

## :busts_in_silhouette: Staffing 
* Who is staffing: 
* passes assigned

## :notebook: Event Content/Copy
* [ ] Copy provided to MPM- we are getting a dedicated respource for this but DRI is in charge of makeing sure deadlines are met
   * [ ] Landing page copy (decide if there will be a landing page and if it will need a meeting setting form)
   * [ ] Pre event email (3-4 weeks before show)
   * [ ] Reminder copy (optional)
   * [ ] Post event emails (1 week before show)

## :microphone: Speaking engagement?
(Description & link to speaker issue)

## :pencil: At Event Meeting Plan (w/ Mktg OPS and FM support)
* [ ] Decide on meeting setting plan with regional FM rep, designated MPM, and EA team (when Exec involved)
* [ ] Target customers/ prospects identified
   * [ ] Share previous lead lists with team on plannign sheet if we have them
   * [ ] Pull speaker list
* [ ] [Client Meeting Prep Template](https://docs.google.com/document/d/1QZB8m99Lt5GVnBDcfST1otmA81uH6EMEMkF-1zhQ0q0/edit#) must be completed for every on site meeting by AE

## :checkered_flag: Post Event w/ ~"MPM"
* [ ] Leads shared with MPM and ops `24 hours after event close`
* [ ] Lead cleaned up (24 hours after event close) and shared in the MktgOps issue
* [ ] List locked (all changes after lock to be made in SFDC) and leads uploaded to SFDC campaign (check on status of upload 24 houts after list shared with ops)
* [ ] Sales notified of lead/contact assignments - auto-assign to match named accounts 
* [ ] After event follow up launched
* [ ] After event survey sent 
* [ ] Event recap (week after) & Feedback collected. [Recap Template](https://docs.google.com/document/d/1qHFOd0TU52Eq-fnYUyMU7E3-FninPciMH9kA7F5yofM/edit)

## :trophy:  Outreach and Follow Up ~"SDR" 
* SDR Manager to assign project lead: (Corp DRI to assign to SDR Manager here)- more on how to pick this person...
* SDR DRI: (SDR manager to fill in)
* SDR Enablement: @megan_odowd 

### Goals for SDR Team (to be filled in by Corp Event DRI)
*SDR project lead, please review the full issue to get an understanding of the event. If you need help with job titles and pulling an event list reach out to the corp event DRI listed at the top of the issue. Please review the handbook to understand the typical SLAs for the below action items.*

- Pre-event SDR goal: 
  - [ ] Registration 
  - [ ] Meetings at event - if applicable
- Post-event SDR goal:
  - [ ] Follow up and SAOs
- Targets:
  - Inclusion Job Titles (Seniority):
  - Inclusion Job Titles (Function):
  - Target number of attendees: 

### Pre Event ~"SDR" (do this 4-6 weeks out from event start)
* [ ] Review the process in the [handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#preparing-for-field-events).
* [ ] Create focus account list of contacts to personally invite. Please include a minimum of 10 accounts. Review the list of attendees and if applicable the SFDC report of last year's attendees linked at the top of this issue. Once your list is compiled, edit the description of this issue and link here. 
* [ ] Clone the appropriate [master sequence](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=6) and add in event-specific information or create a more personalized sequence. Please link the sequence that will be used here.
* [ ] Send the new sequence to @megan_odowd to add it to the Outreach collection. 
* [ ] Work with your manager and, if needed the Corp Event DRI, to determine the best day to start the sequence. Enable any other SDR reps assisting with this.
* [ ] Update event sheet with all meetings set. **NOTE** pull event sheet from the :paperclip: Event Planning Spreadsheet & Recap above. 

### Post Event for ~"SDR" 
##### Follow up: planned from Corp Events: 
DRI to pull in link from `Follow-up email` MPM issue, so everyone is aligned on the follow-up message and the offer that is being made. Please delete this text and replace it with the link once the issue has been created. 

##### Follow up: needed from SDR and Sales team:
(Description)



/assign @nbsmith 

/label ~Events ~"Field Marketing" ~"MktgOps - FYI" ~"MPM - Radar" ~"Marketing Programs" ~"Marketing Campaign" ~"Corporate Event" ~"mktg-status::plan" ~"META"
